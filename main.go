package main

import (
	"fmt"
	"gitlab.com/derbes/week2homework"
	"math/rand"
	"time"
)

func main(){

	//Bubble sort using

	slc:= []int{} // our empty slice

	for i:=0;i<50000;i++{
		slc = append(slc, rand.Int()%100)
	}
	// randomly fill slice

	timeIn := time.Now() // Time before sorting
	sorted:= week2homework.Bsort(&slc)
	timeOut:= time.Now() // Time after sorting
	elapse := timeOut.Sub(timeIn)  // difference of time, means how much time worked our bubble sort

	fmt.Println(elapse)
	fmt.Println(sorted[0])
	// because slc contain 50000 elements, that means 50000^2 it approximately 2 sec

	//for i:=0; i<len(slc);i++{
	//	fmt.Println(sorted[i])
	//}
}